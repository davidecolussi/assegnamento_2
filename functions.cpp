//Commento iniziale
//commento fatto con sublimetext
//commento nuovo

#include "model.h"
#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <sstream>

//funzione per leggere gli ordini e il cash dal file orders.dat
std::vector<Order> readOrders(std::string path,double& cash) {		
	std::string line;
	std::vector<Order> v;
	std::ifstream myfile(path);
	if (myfile.is_open())
	{
		bool isFirstLine = true;
		std::vector<std::string> words;
		while (getline(myfile, line))
		{
			std::string word = "";
			for (auto x : line)
			{
				if ((x == '\t') || (x == ' ') || (x == '[') || (x == ']')) // \t = rientro
				{
					if (word.length() > 0) {
						words.push_back(word);
					}
					word = "";
				}
				else
				{
					word = word + x;
				}
			}
			if (isFirstLine) {
				isFirstLine = false;
				cash = std::stod(words[0]);
				words.clear();
			}
			else {
				int month = std::stoi(words[0]);
				std::string id = words[1];
				int quantity = std::stoi(words[2]);
				v.push_back(Order(month, id, quantity));
				words.clear();
			}
		}
		myfile.close();
	}
	else std::cout << "Unable to open file";
	return v;
}

//funzione per leggere i componenti dal file components_info.dat(?)
std::vector<Component> readComponents(std::string path) {	
	std::string line;
	std::vector<Component> v;
	std::ifstream myfile(path);
	if (myfile.is_open())
	{
		std::vector<std::string> words;
		while (getline(myfile, line))
		{
			std::string word = "";
			for (auto x : line)
			{
				if ((x == '\t') || (x == ' ') || (x == '[') || (x == ']'))
				{
					if (word.length() > 0) {
						words.push_back(word);
					}
					word = "";
				}
				else
				{
					word = word + x;
				}
			}
			std::string id = words[0];
			std::string name = words[1];
			int time= std::stoi(words[2]);
			double price1 = std::stod(words[3]);
			double price2 = std::stod(words[4]);
			double price3 = std::stod(words[5]);
			v.push_back(Component(id,name,time,price1,price2,price3));
			words.clear();
		}
		myfile.close();
	}
	else std::cout << "Unable to open file";
	return v;
}

//funzione per leggere dal file models.dat le path dei file modelX.dat
std::vector<std::string> readModelPaths(std::string path) {		
	std::string line;
	std::vector<std::string> paths;
	std::ifstream myfile(path);
	if (myfile.is_open())
	{
		while (getline(myfile, line))
		{
			std::string word = "";
			for (auto x : line)
			{
				if ((x == '\t') || (x == ' ') || (x == '[') || (x == ']'))
				{
					if (word.length() > 0) {
						paths.push_back(word);
					}
					word = "";
				}
				else
				{
					word = word + x;
				}
			}
		}
		myfile.close();
	}
	else std::cout << "Unable to open file";
	return paths;
}

//funzione per leggere i modelli dai diversi file dat
std::vector<Model> readModel(std::vector<std::string> paths, std::vector<Component> componentsList) {	
	std::string line;
	std::vector<Model> v;
	for (int i = 0; i < paths.size(); i++) {
		std::ifstream myfile(paths[i]);
		std::vector<Component> componentOfThisModel;
		std::vector<int> amountComponent;
		std::string id;
		std::string name;
		int price;
		if (myfile.is_open())
		{
			bool isFirstLine = true;
			bool isSecondLine = false;
			std::vector<std::string> words;
			while (getline(myfile, line))
			{
				std::string word = "";
				for (auto x : line)
				{
					if ((x == '\t') || (x == ' ') || (x == '[') || (x == ']'))
					{
						if (word.length() > 0) {
							words.push_back(word);
						}
						word = "";
					}
					else
					{
						word = word + x;
					}
				}
				if (isFirstLine) {
					isFirstLine = false;
					isSecondLine = true;
					price = std::stoi(words[0]);
					words.clear();
				}
				else {
					if (isSecondLine) {
						isSecondLine = false;
						id = words[0];
						name = words[1];
						words.clear();
					}
					else {
						for (int k = 0; k < componentsList.size(); k++) {
							if (words[0].compare(componentsList[k].id) == 0) {
								componentOfThisModel.push_back(Component(words[0], words[1], componentsList[k].deliveryTime, componentsList[k].cost1, componentsList[k].cost2, componentsList[k].cost3));
								amountComponent.push_back(std::stoi(words[2]));
							}
						}
					}
				}
				words.clear();
			}
			myfile.close();
			v.push_back(Model(id, name, componentOfThisModel, amountComponent, price));
		}
		else std::cout << "Unable to open file";
	}
	return v;
}

std::ostream& operator<<(std::ostream& os, Component c) {
	os << c.id << " " << c.name << " " << c.deliveryTime << " " << c.cost1 << " " << c.cost2 << " " << c.cost3;
	return os;
}

std::ostream& operator<<(std::ostream& os, Order o) {
	os << o.month << " " << o.id << " " << o.quantity;
	return os;
}

std::ostream& operator<<(std::ostream& os, Model m) {
	os << m.id << " " << m.name<<" "<< m.priceToSell<<"\n";
	for (int i = 0; i < m.components.size(); i++) {
		os << m.components[i]<<" "<<m.amount[i]<<"\n";
	}
	return os;
}

bool hasSameComponents(std::vector<Component>& c1, std::vector<Component>& c2) {		//guarda se hanno componenti in comune, non l'ho mai usata
	for (int i = 0; i < c1.size(); i++) {
		for (int k = 0; k < c2.size(); k++) {
			if (c1[i].id.compare(c2[k].id) == 0) {
				return true;
			}
		}
	}
	return false;
}