#pragma once

#include <vector>
#include <string>
#include <iostream>

struct Component {
	std::string id;
	std::string name;
	int deliveryTime;
	double cost1;
	double cost2;
	double cost3;
	int quantity=0;
	Component(std::string i, std::string n, int t, double c1, double c2, double c3) :id{ i }, name{ n }, deliveryTime{ t }, cost1{ c1 }, cost2{ c2 }, cost3{ c3 }{};
};

struct Order { //è UN singolo ordine di UN modello
	int month;
	std::string id;
	int quantity;
	Order(int m, std::string i, int q) :month{ m }, id{ i }, quantity{ q }{};
};

struct Model {
	int priceToSell;
	std::string id;
	std::string name;
	std::vector<Component> components; //vettore di id dei components
	std::vector<int> amount; //vettore di quantita' di ogni component
	Model(std::string i, std::string n, std::vector<Component> c, std::vector<int> a, int p) :id{ i }, name{ n }, components{ c }, amount{ a }, priceToSell{ p }{};
};

//funzioni che devono essere implementate in functions.cpp e poi utilizzate nel main

std::vector<Component> readComponents(std::string);
std::vector<std::string> readModelPaths(std::string);
std::vector<Model> readModel(std::vector<std::string>, std::vector<Component> );
std::vector<Order> readOrders(std::string, double&);
std::ostream& operator<<(std::ostream&, Component);
std::ostream& operator<<(std::ostream&, Order);
std::ostream& operator<<(std::ostream&, Model);
bool hasSameComponents(std::vector<Component>&, std::vector<Component>&);
