#include <iostream>
#include <string>
#include <fstream>
#include <chrono>
#include <thread>
#include "model.h"
//prova testing


// -------- GESTIONE DEGLI ORDINI DA FARE IL MESE CORRENTE: ----------


// funzione che si occupa di capire come gestire gl ordini da eseguire questo mese, preoccupandosi di spendere il meno possibile
// e eseguire gli ordini in un ordine che sia efficiente
//
// parametri passati:
// il mese, gli ordini, la lista dei modelli della nostra azienda, la cassa attuale

void getThisMonthOrders(int month,const std::vector<Order>& orders, const std::vector<Model>& modelsList, double cash) {
    std::vector<Model> orderModels;
    std::vector<int> orderQuantities;
    for (int i = 0; i < orders.size(); i++) {
        if (orders[i].month == month) {
            for (int k = 0; k < modelsList.size(); k++) {
                if (orders[i].id.compare(modelsList[k].id) == 0) {
                    orderModels.push_back(modelsList[k]);       //popolo orderModels (vect di Model): contiene i modelli
                    orderQuantities.push_back(orders[i].quantity);      //popolo orderQuantities (vect di interi): contiene la quantita' per ogni modello
                }
            }
        }
    }
    if (orderModels.size() == 0) {
        return;
    }

    /*IDEA PER SCEGLIERE COSA ORDINARE
        Se ho abbastanza cassa a disposizione per completare tutti gli ordini li effettuo tutti. Se mi rimane una buona parte di cassa da parte
        posso acquistare all'ingrosso, in previsione futura, i componenti che mi servivano di pi� nell'ordine appena iniziato.
        Ovviamente se ho da parte gia un magazzino con tanti di quei componenti non li ordino, starebbero li a fare la muffa.
        Ultima cosa, se non ho abbastanza soldi per un ordine, non lo effettuo. Prediligo in generale gli ordini "pi� all'ingrosso" per guadagnare di piu
    */

    //ORDINAMENTO DEGLI ORDINI DI QUESTO MESE

    double totalcost = 0;   //costo totale degli ordini questo mese
    for (int i = 0; i < orderModels.size();i++) {
        //ciclo for per ogni modello
        double componentCost = 0;   //costo totale dei componenti (variabile temporanea)
        int k = 0; //indice per il prossimo for
        for (Component c : orderModels[i].components) { 
            //per ogni componente di un modello ordinato guardo le quantita' totali che mi servono 
            //per vedere i costi se sono all'ingrosso e calcolo gli opportuni prezzi
            //ciclo per ogni componente
            if (orderQuantities[i] * orderModels[i].amount[k] < 11) {
                //se ho meno di 11 componenti di questo tipo da ordinare...
                componentCost += c.cost1 * orderModels[i].amount[k];
            }
            else if ((orderQuantities[i] * orderModels[i].amount[k]>10) && (orderQuantities[i] * orderModels[i].amount[k] < 51)) {
                //se ho tra 11 e 50 componenti di questo tipo da ordinare...
                componentCost += c.cost2 * orderModels[i].amount[k];
            }
            else if (orderQuantities[i] * orderModels[i].amount[k] > 50) {
                //se ho piu' di 50 componenti di questo tipo da ordinare...
                componentCost += c.cost3 * orderModels[i].amount[k];
            }
            k++;
        }
        totalcost += componentCost * orderQuantities[i]; //aggiorno il costo totale degli ordini di questo mese
    }
    if (cash > totalcost) {
        std::cout << "TUTTI GLI ORDINI POSSONO ESSERE EFFETTUATI\n";
    }

    /*for (int i = 0; i < orderModels.size(); i++) {
        for (int k = i+1; k < orderModels.size(); k++) {
            if (hasSameComponents(orderModels[i].components, orderModels[k].components)) {

            }
        }
    }*/
}


// -.-.-.----- FINE GESTIONE DEGLI ORDINI DA FARE IL MESE CORRENTE: -------.-.-.-












// --------- MAIN ---------

int main()
{
    int actualMonth = 0; //mese corrente
    double cash = 0;    //soldi iniziali


    // --------- OPERAZIONE DI LETTURA INIZIALE DAI FILE ----------

    //percorsi dei file .dat:
    std::string pathOrders = "orders.dat";
    std::string pathComponents = "components_info.dat";
    std::string pathModels = "models.dat";

    std::vector<Component> componentsListActual = readComponents(pathComponents);   //lista coi componenti attuali in magazzino
    std::vector<Order> orders = readOrders(pathOrders, cash);                        //lista degli ordini, aggiorno cash passato x riferimento 
    std::vector<std::string> modelPaths = readModelPaths(pathModels);                   //lista dei path dei file dei modelli
    std::vector<Model> modelsList = readModel(modelPaths, componentsListActual);     //lista dei modelli disponibili con relativi componenti

    //ho letto i componenti attuali in magazzino -> componentListActuals
    //ho letto gli ordini                        -> orders
    //ho letto i percorsi dei file dei modelli   -> modelPaths
    //ho letto i modelli                         -> modelsList


    // -.-.-.------ FINE OPERAZIONE DI LETTURA INIZIALE DAI FILE -------.-.-.-

    
    //STAMPA DEL VETTORE componentsListActuals

    std::cout<<"\nSTAMPA DEL VETTORE componentListActuals:\n";
    for(int i = 0; i<componentsListActual.size(); i++)
        std::cout<<componentsListActual.at(i)<<"\n";


    //SIMULAZIONE DELLO SCORRERE DEI MESI: ogni secondo passa un mese

    std::cout<<"\nSIMULAZIONE DEL PASSAGGIO DEI MESI:\n";
    while (1){ 
        std::cout <<"MESE "<<actualMonth<<":\n";
        getThisMonthOrders(actualMonth,orders,modelsList,cash);
        std::this_thread::sleep_for(std::chrono::seconds(1));
        actualMonth++;
    }
    
    
    
    /*PRINT ORDERS AND COMPONENTS
    for (int i = 0; i < orders.size(); i++) {
        std::cout << orders[i]<<"\n";
    }
    for (int i = 0; i < componentsList.size(); i++) {
        std::cout << componentsList[i]<<"\n";
    }
    std::cout<<cash;
    for (int i = 0; i < modelPaths.size(); i++) {
        std::cout << modelPaths[i]<<"\n";
    }
    for (int i = 0; i < modelsList.size(); i++) {
        std::cout << modelsList[i]<<"\n";
    }
    */

    return 0;
}


// -.-.-.------ FINE MAIN -------.-.-.-
